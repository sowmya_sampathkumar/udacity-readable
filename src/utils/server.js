const server = 'http://localhost:3001'

let auth = localStorage.auth

if (!auth) 
  auth = localStorage.auth = Math.random().toString(36).substr(-8)

const headers = {
  'Accept': 'application/json',
  'Authorization': auth
}

export function getAllCategories () {
  return fetch(`${server}/categories`, { headers })
    .then(res => res.json())
    .then(data => data.categories)
}

export function getAllBlogs () {
  return fetch(`${server}/posts`, { headers })
    .then(response => response.json())
}

export function addNewBlog (newBlog) {
  return fetch(`${server}/posts`, { 
    method: 'POST',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newBlog)
  })
  .then(data => data.json())
}

export function getOneBlog (id) {
  return fetch(`${server}/posts/${id}`, { headers })
    .then(response => response.json())
}

export function editBlog (id, blog) {
  return fetch(`${server}/posts/${id}`, {
    method: 'PUT',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(blog)
  })
  .then(data => data.json())
}

export function voteBlog (id, option) {
  return fetch(`${server}/posts/${id}`, {
    method: 'POST',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      option: option
    })
  })
  .then(data => data.json())
}

export function deleteBlog (id) {
  return fetch(`${server}/posts/${id}`, { 
    method: 'DELETE',
    headers 
  })
}

export function getComments (id) {
  return fetch(`${server}/posts/${id}/comments`, { headers })
    .then(response => response.json())
}

export function addComment (newComment) {
  return fetch(`${server}/comments`, { 
    method: 'POST',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(newComment)
  })
  .then(data => data.json())
}

export function deleteComment (id) {
  return fetch(`${server}/comments/${id}`, {
    method: 'DELETE',
    headers
  })
  .then(data => data.json())
}

export function editComment (id, comment) {
  return fetch(`${server}/comments/${id}`, {
    method: 'PUT',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(comment)
  })
  .then(data => data.json())
}

export function voteComment (id, option) {
  return fetch(`${server}/comments/${id}`, {
    method: 'POST',
    headers: {
      ...headers,
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      option: option
    })
  })
  .then(data => data.json())
}
