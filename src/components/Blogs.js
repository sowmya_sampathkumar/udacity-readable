import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { getBlogs } from '../actions'

class Blogs extends Component {
  componentDidMount() {
    this.props.getBlogs()
  }

  render() {
    console.log(this.props.blogs)

    const {blogs}  = this.props
    const {filter}  = this.props
    const {sort} = this.props

    switch (sort) {
      case 'vote':
        blogs.sort((a, b) => a.voteScore < b.voteScore)
        break;
      case 'time':
        blogs.sort((a, b) => a.timestamp < b.timestamp)
        break;
      default:
        break;
    }
    return(
      <div className="Blogs">
        <ul id = "blogs" className="Blogs-List">
          {blogs.map((blog, index) => {
            var bdate = new Date(blog.timestamp);
            if (filter !== 'all')  {
              if (blog.category === filter) {
                return (
                  <div key={"div"+index}>
                    <li key={index}><Link to={`/blog/${blog.id}`}>{blog.title}</Link></li>
                    <li key={index+'1'}><b>Created by : </b>{blog.author} <b>on</b> {bdate.toString()} <b>under</b> {blog.category}</li>
                    <li key={index+'2'}>{blog.body}</li>
                    <li key={index+'3'}>Votes = {blog.voteScore} </li>
                    <hr/>
                  </div>
                )
              }
            } else {
              return (
              <div key={"div"+index}>
                <li key={index}><Link to={`/blog/${blog.id}`}>{blog.title}</Link></li>
                <li key={index+'1'}><b>Created by : </b>{blog.author} <b>on</b> {bdate.toString()} <b>under</b> {blog.category}</li>
                <li key={index+'2'}>{blog.body}</li>
                <li key={index+'3'}>Votes = {blog.voteScore} </li>
                <hr/>
              </div>)
            }
          })}
        </ul>
      </div>
    )
  }
}
const mapStateToProps = ({ blogs }) => {
  return {
    blogs: blogs.blogs,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getBlogs: () => dispatch(getBlogs())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Blogs)
