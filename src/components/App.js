import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom'

import '../style/App.css';

import MainPage from './MainPage.js'
import NewBlog from './AddBlog.js'
import EditBlog from './EditBlog.js'
import ViewBlog from './ViewBlog.js'
import EditComment from './EditComment.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <h2>Udacity Readable Project </h2>
        <hr />
        <Switch>
          <Route exact path ='/' component={MainPage} />
          <Route exact path ='/new' component={NewBlog} />
          <Route exact path ='/blog/:id' component={ViewBlog} />
          <Route exact path ='/edit/:id' component={EditBlog} />
          <Route exact path ='/comment/:index' component={EditComment} />
        </Switch>
      </div>
    );
  }
}

export default App;
