import React, { Component } from 'react'
import { connect } from 'react-redux'
import { fetchCategories } from '../actions'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class Menu extends Component {
  componentDidMount() {  
    this.props.getCategories()
  }

  setFilterClick(type) {
    this.props.filter(type)
  }

  render() {    
    const { categories } = this.props
    return(
      <div className="Categories">
        <span>Click on any category to filter by -></span>
        {categories.map((item, index) => {
          const catgName = item.name
          return (
            <input key= {index} type="button" style={elmStyle} onClick={this.setFilterClick.bind(this, catgName )} value={item.name}></input>
          )
        })}
        <input key= 'all' className="All-Button" type="button" style={elmStyle} onClick={this.setFilterClick.bind(this, 'all')} value="All"></input>
      </div>
    )
  }
}

const mapStateToProps = ({ categories }) => {
  return {
    categories: categories.categories,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCategories: () => dispatch(fetchCategories())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu)
