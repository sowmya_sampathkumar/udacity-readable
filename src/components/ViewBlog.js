import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { getBlog, addCommentAction, upVoteAction, downVoteAction, deleteBlogAction } from '../actions'
import CommentList from './Comments.js'

const elmStyle = {
  verticalAlign:'middle',
  margin:'10px',
};

class ViewPost extends Component {
  state = {
    blogId: '',
    author: '',
    body: '',
    deleted: false,
    id:'',
    parentDeleted: false,
    parentId: '',
    timestamp: '',
    PostvoteScore: 1,
  }

  componentDidMount() {
    const { id } = this.props.match.params
    this.props.getPost(id);
    this.setState({ blogId: id })
  }

  onUpPostClick() {
    this.props.addVote(this.state.blogId)
      .then(() => {
        alert("Up Voted successfully")
        const { id } = this.props.match.params
        this.props.getPost(id);
      })
  }

  onDnPostClick() {
    this.props.delVote(this.state.blogId)
    .then(() => {
      alert("Down voted successfully")
      const { id } = this.props.match.params
      this.props.getPost(id);
    })
  }

  onDelPostClick() {
    this.props.delPost(this.state.blogId)
      .then(
        alert("Post deleted successfully")
      )
      var newPath = '/';
      this.props.history.push(newPath)
  }

  onHomeClick() {
    var newPath = '/';
    this.props.history.push(newPath)
  }

  onAddCommentClick() {
    let { body } = this.state

    if (body) {
      const addComment = {
        author: this.state.author,
        body: this.state.body,
        deleted: false,
        id: Math.random().toString(36).substr(-8),
        parentDeleted: false,
        parentId: this.state.parentId,
        timestamp: Date.now(),
        voteScore: 1,
      } 
      this.props.addComment(addComment)
        .then(() => this.setState({
          author: '',
          body: '',
          deleted: false,
          id:'',
          parentDeleted: false,
          parentId: '',
          timestamp: '',
          voteScore: 1,
        }))
        alert('Comment added successfully');
      } else {
      alert('Please input some comment');
    }
  }

  onBodyChange(e) {
    this.setState({ body: e.target.value })
  }

  render() {
    const { blog } = this.props.blog
    this.state.author = blog.author
    this.state.parentId = blog.id

    var bdate = new Date(blog.timestamp);
    return(
      <div className="SelectedPost">
        <ul id = "blogs">
          <input className="Hm-Button" type="button" onClick={this.onHomeClick.bind(this)} value="Home page"></input>
          <hr />
          <li key={blog.id}>{blog.title}</li>
          <li>Created by : {blog.author} on {bdate.toString()}</li>
          <li>{blog.body}</li>
          <li>Votes = {blog.voteScore} </li>
          <button><Link to={`/edit/${blog.id}`}>Edit Post</Link></button>
          <input className="Del-Button" type="button" style={elmStyle} onClick={this.onDelPostClick.bind(this)} value="Delete Blog"></input>
          <input className="Uv-Button" type="button" style={elmStyle} onClick={this.onUpPostClick.bind(this)} value="Up vote"></input>
          <input className="Dv-Button" type="button" style={elmStyle} onClick={this.onDnPostClick.bind(this)} value="Down vote"></input>
          <hr/>
        </ul>
        <textarea placeholder="Add comments here..." value={this.state.body} name="body" onChange={(e) => this.onBodyChange(e)} style={elmStyle} cols="100" rows="6"> </textarea>
        <input className="Post-Button" type="button" style={elmStyle} onClick={this.onAddCommentClick.bind(this)} value="Add comment"></input>
        <hr/>
        <CommentList comments={blog.comments} />
      </div>
    )
  }
}

const mapStateToProps = ({blog, comment,blogs }) => {
  return {
    blog: blog,
    comment: comment,
    blogs: blogs,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getPost: (id) => dispatch(getBlog(id)),
    addComment: (comment) => dispatch(addCommentAction(comment)),
    addVote: (id) => dispatch(upVoteAction(id)),
    delVote: (id) => dispatch(downVoteAction(id)),
    delPost: (id) => dispatch(deleteBlogAction(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPost)
